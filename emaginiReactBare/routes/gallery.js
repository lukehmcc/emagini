/*
 This is an app created by a literal god
 */
import React, { Component } from "react";
import {View, Text, StyleSheet, TouchableOpacity, Alert, Dimensions, Image, FlatList } from "react-native";
import Video from 'react-native-video';
import Orientation from 'react-native-orientation-locker';

//defines some stuff
const isImage = require('is-image');

//Does the picking
export default class EmaginiPhotoGallery extends Component {
    state = {
        data:[
            require('../assets/images/mountains.jpg'),
            require('../assets/images/river.jpg'),
            require('../assets/images/1.jpg'),
            require('../assets/images/2.jpg'),
            require('../assets/images/3.jpg'),
            require('../assets/images/4.jpg'),
            require('../assets/images/5.jpg'),
            require('../assets/images/6.jpg'),
            require('../assets/images/7.jpg'),
            require('../assets/images/8.jpg'),
            require('../assets/images/9.jpg'),
            require('../assets/images/10.jpg'),
            require('../assets/images/11.jpg'),
            require('../assets/images/12.jpg'),
            require('../assets/images/13.jpg'),
            require('../assets/images/14.jpg'),
            require('../assets/images/15.jpg'),
            require('../assets/images/16.jpg'),
            require('../assets/images/17.jpg'),
            require('../assets/images/18.jpg'),
            require('../assets/images/19.jpg'),
            require('../assets/images/20.jpg'),
            require('../assets/images/21.mp4'),
            require('../assets/images/bo.png'),
        ]
      }
    
    componentDidMount() {
        Orientation.lockToPortrait();
    }
    
    renderItem(item){
        Alert.alert(item);
            return(
                <TouchableOpacity
                    style={styles.photoStyle}
                    onPress={() => this.props.navigation.push('ImageViewer', {itemPass: item})}>
                    <Image source={item} style={styles.photoStyle} />
                </TouchableOpacity>
            );
        
        // }else{
        //     return(
        //         <TouchableOpacity
        //             style={styles.photoStyle}
        //             onPress={() => this.props.navigation.push('VideoViewer', {itemPass: item})}>
        //             <Video source={item}   // Can be a URL or a local file.
        //                 ref={(ref) => {
        //                 this.player = ref
        //                 }}                                      // Store reference
        //                 onBuffer={this.onBuffer}                // Callback when remote video is buffering
        //                 onError={this.videoError}               // Callback when video cannot be loaded
        //                 style={styles.backgroundVideo}
        //                 muted={true} />
        //         </TouchableOpacity>
        //     );
        // }
    }
    render() {
        
        return (
            <View>
                <FlatList
                    data={this.state.data}
                    numColumns= {3}
                    renderItem={({ item }) => this.renderItem(item)}
                    keyExtractor={(item, index) => "" + index}
                />
            </View>
        );
    }
 }

const styles = StyleSheet.create({
    photoStyle: {
        aspectRatio: 1,
        margin: 0,
        height: null,
        flex: 1,
        margin: '.5%',
    },
});
