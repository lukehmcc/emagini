/*
 This is an app created by a literal god
 */
import React, { Component } from "react";
import {View, Text, StyleSheet, Dimensions, Image, Modal } from "react-native";
import ReactNativeZoomableView from '@dudigital/react-native-zoomable-view/src/ReactNativeZoomableView';



//Does the picking
export default class EmaginiPhotoViewer extends Component {
    state = {
        data:[
            require('../assets/images/mountains.jpg'),
        ]
      }
    // IMAGE VIEWER
    render() {
        // Grabs the image/video from the router
        const image2 = this.props.navigation.state.params.itemPass;
        // Then tests if it's an image to decider which viewer to use
        return (
            
            <View style={styles.container}>
                <ReactNativeZoomableView
                    maxZoom={ 2 }
                    minZoom={ 1 }
                    zoomStep={ 0.5 }
                    initialZoom={ 1 }
                    bindToBorders={true}
                    onZoomAfter={this.logOutZoomState}
                    style={StyleSheet.container}>
                        <Image style={styles.imageStyle} source={image2} />
                    </ReactNativeZoomableView>
            </View>
        );
    }
 }

const styles = StyleSheet.create({
    imageStyle: {
        flex: 1,
        width: '100%',
        resizeMode: 'contain',
    },
    container: { 
        flex: 1,
    },
});
