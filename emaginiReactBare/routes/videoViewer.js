/*
 This is an app created by a literal god
 */
import React, { Component } from "react";
import {View, Text, StyleSheet, Dimensions, Image, Modal, Alert } from "react-native";
import ReactNativeZoomableView from '@dudigital/react-native-zoomable-view/src/ReactNativeZoomableView';
import VideoPlayer from 'react-native-video-controls';
import Orientation from 'react-native-orientation-locker';


//Does the picking
export default class EmaginiVideoViewer extends Component {
    // Hides the header for stack navigator
    static navigationOptions = { headerShown: false }

    constructor(props) {
        super(props);
    
        this.state = {
            isFullscreen: this.props.isFullscreen || false,
            data: [
                require('../assets/images/21.mp4'),
            ],
        };
    }

    // Switcher for fullscreen 
    enterFullScreen() {  
        Orientation.lockToLandscape();
    };  
    exitFullScreen() {
        Orientation.lockToPortrait();
    };
    
    // IMAGE VIEWER
    render() {
        // const video2 = this.props.navigation.state.params.itemPass; saving this for later
        return (  
            <View style={styles.videoStyle}> 
                <VideoPlayer
                    source={this.state.data[0]}
                    navigator={this.props.navigation}
                    tapAnywhereToPause={true}
                    toggleResizeModeOnFullscreen={false}
                    onEnterFullscreen={this.enterFullScreen}
                    onExitFullscreen={this.exitFullScreen} />
            </View>
        );
    }
 }

const styles = StyleSheet.create({
    videoStyle: {
        flex: 1,
        width: '100%',
        alignSelf: 'center',
        flexDirection: 'row',
    },
    fullscreen: {
        // Style in a way that it's fullscreen based on your app
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        top: 0,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
