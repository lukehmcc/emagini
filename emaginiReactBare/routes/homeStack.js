import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import EmaginiStartScreen from './index';
import EmaginiPhotoGallery from './gallery';
import EmaginiImageViewer from './imageViewer';
import EmaginiVideoViewer from './videoViewer';

const screens = {
    Launch: {
        screen: EmaginiStartScreen,
        navigationOptions: {
            headerShown: false,
          }
    },
    Gallery: {
        screen: EmaginiPhotoGallery,
        navegationOptions: {
            headerShown: false,
        }
    },
    ImageViewer: {
        screen: EmaginiImageViewer,
        navegationOptions: {
            headerShown: true,
        }
    },
    VideoViewer: {
        screen: EmaginiVideoViewer,
    }
}

const HomeStack = createStackNavigator(screens);

export default createAppContainer(HomeStack);