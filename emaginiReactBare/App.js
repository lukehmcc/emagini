/*
 This is an app created by a literal god
 */
// Import the things
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    View,
    Text,
    StatusBar,
    Image,
    ImageBackground,
    Button,
} from 'react-native';
import * as Font from 'expo-font';
import { PermissionsAndroid } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import Navegator from './routes/homeStack'; // Grabs that staack


export default class Emagini extends Component {
    constructor(props){
        super(props);
        this.state = { loading: true };
    }
    async requestPhotosPermission() {
        try {
          const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE)
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              this.getPhotos();
            } else {
              console.log("Photos permission denied")
            }
        } catch (err) {
          console.warn(err)
        }
    }
    async componentDidMount() {
      await Font.loadAsync({
        regular: require('./assets/fonts/Ubuntu-R.ttf'),
        bold: require('./assets/fonts/Ubuntu-B.ttf'),
        light: require('./assets/fonts/Ubuntu-L.ttf'),
      });
      this.setState({ loading: false });
    }

    render(){
        if (this.state.loading){
            return (
                <View style={styles.container}>
                    <View style={styles.buffer} />
                    <Text style={styles.text}> Give me a sec... </Text>
                    <View style={styles.buffer} />
                </View>
            );
        }
        else{
            return (
                <Navegator />
            );
        }
    }
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    flexDirection: 'column',
},
buffer: {
    flex: .45,
},
text: {
    flex: .1,
    fontSize: 30,
    alignSelf: 'center',
},
});